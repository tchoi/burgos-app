package com.example.jonas.burgosteste;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends BaseActivity implements View.OnClickListener{
    protected static String TAG = LoginActivity.class.getSimpleName();

    private EditText etEmail;
    private EditText etSenha;
    private AppCompatButton btnlogin;
    private TextView tvSignup;
    MyProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        context = this;
    }

    private void initView() {
        progressDialog = new MyProgressDialog(this);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);
        btnlogin = (AppCompatButton) findViewById(R.id.btnlogin);
        tvSignup = (TextView) findViewById(R.id.tvSignup);

        btnlogin.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
    }

    protected void validate(){

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnlogin:{
                validate();
                break;
            }
            case R.id.tvSignup:{
                Helper.goTo(context,SignUpActivity.class);
                break;
            }
        }
    }
}
