package com.example.jonas.burgosteste;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;

public class BaseActivity extends AppCompatActivity {
    protected Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }

    protected void alert(String title, String msg) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void alert(String title, String msg, final Class<?> classe) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent it = new Intent(context,classe);
                        startActivity(it);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }
}
