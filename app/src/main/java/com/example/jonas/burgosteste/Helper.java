package com.example.jonas.burgosteste;

import android.content.Context;
import android.content.Intent;

public class Helper {
    public static void goTo(Context lcontext, Class<?> lcls) {
        Intent intent = new Intent(lcontext, lcls);
        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        lcontext.startActivity(intent);
    }
}
